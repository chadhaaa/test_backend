const sonarqubeScanner = require('sonarqube-scanner')
sonarqubeScanner(
	{
		serverUrl: 'http://localhost:9000',
		options: {
			'sonar.sources': '.',
			'sonar.tests': '.',
			'sonar.inclusions': '.',
			'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info',
			'sonar.testExecutionReportPaths': 'coverage/test-reporter.xml',
			'sonar.language': 'js',
		},
	},
	() => {}
)

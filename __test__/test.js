const server = require('../server.js')
const supertest = require('supertest')
const { response } = require('../server.js')
const requestWithSupertest = supertest(server)

describe('Coach and Player Endpoints', () => {
	//test get all challenges youssef_CrudChallenge.test.js

	//test update one challenge youssef_CrudChallenge.test.js

	//test post challenges youssef_CrudChallenge.test.js

	//test get events youssef_CrudEvent.test.js

	//test Post event youssef_CrudEvent.test.js

	//test get listeSession youssef_ListSession.test.js

	//test Post session youssef_session.test.js

	// test GET ALL COMPETENCES chadha_hajji_crudComp.js
	it('GET api/competences should show all competences', async () => {
		const res = await requestWithSupertest.get('/api/competences')
		expect(res.status).toEqual(200)
	})
	// test GET ALL STATISTICS chadha_hajji_crudStat.js
	it('GET api/statistics should show all statistics', async () => {
		const res = await requestWithSupertest.get('/api/statistics')
		expect(res.status).toEqual(200)
	})
	// test ADD COMPETENCE chadha_hajji_crudComp.js
	it('POST /api/competence should add a competence', async () => {
		const competence = {
			name: 'Running',
			description: 'Run 12km',
			link: 'www.youtube.com',
			visibility: 'true',
			stars: 3,
		}
		const competenceRes = await requestWithSupertest
			.post('/api/competence')
			.send(competence)

		expect(competenceRes.statusCode).toEqual(200)
		const res = await requestWithSupertest.get(`/api/competence/${competenceRes.body._id}`)
		expect(competenceRes.body.name).toEqual('Running')
	})
	// test ADD STATISTIC chadha_hajji_crudStat.js
	it('POST /api/statistic should add a statistic', async () => {
		const statistic = {
			type: 'Timer',
			unit: 'km',
			title: 'Squats',
			description: 'Do 100 squats',
			link: 'www.fitnessApp.com',
			visibility: 'true',
			currentState: '56',
			minMax: 'Minimiser',
			statAlert: 'false',
		}
		const statisticRes = await requestWithSupertest.post('/api/statistic').send(statistic)

		expect(statisticRes.statusCode).toEqual(200)
		const res = await requestWithSupertest.get(`/api/statistic/${statisticRes.body._id}`)
		expect(statisticRes.body.title).toEqual('Squats')
	})
	// test Invite Player chadha_hajji_invitePlayer.js
	it('POST /api/players should invite a player', async () => {
		const invitePlayer = {
			firstname: 'chadha',
			lastname: 'hajji',
			email: 'chadha.hadji@gmail.com',
			sessionPrice: '10',
			sessionNumbers: '3',
			password: '12345678',
		}
		const invitePlayerRes = await requestWithSupertest
			.post('/api/players')
			.send(invitePlayer)

		expect(invitePlayerRes.statusCode).toEqual(200)
		const res = await requestWithSupertest.get(`/api/players/${invitePlayerRes.body._id}`)
		expect(invitePlayerRes.body.email).toEqual('chadha.hadji@gmail.com')
	})
	// test GET ONE STATISTIC chadha_hajji_crudStat.js

	// test GET ONE COMPETENCE chadha_hajji_crudComp.js

	// test ONE SESSION chadha_hajji_sessionDetails.js

	// test GET PROFILE PLAYER BY ID chadha_hajji_viewProfile.js

	// test UPDATE COMPETENCE chadha_hajji_crudComp.js

	// test UPDATE STATISTIC chadha_hajji_crudStat.js
	/*it('PUT /api/statistic/:id should update a statistic', async  => {
		const statisticUpdate = '62727aa238785c8b4cd4890f'
		const statisticRes = await requestWithSupertest.put(
			`/api/statistic/${statisticUpdate}`
		).send({})
		
		expect(statisticRes.statusCode).toEqual(200)
	})*/
	// test DELETE STATISTIC chadha_hajji_crudStat.js
	// it('DELETE /api/statistic/:id should delete a statistic', async  => {
	// 	const statisticDelete = '6292c9794de8f1f6d76060b0'
	// 	const statisticDeleteRes = await requestWithSupertest.delete(
	// 		`/api/statistic/${statisticDelete}`
	// 	)
	//
	// 	expect(statisticDeleteRes.statusCode).toEqual(200)
	// })
	// test DELETE COMPETENCE chadha_hajji_crudComp.js
	// it('DELETE /api/competence/:id should delete a competence', async  => {
	// 	const competenceDelete = '623c88f9e5cec2f3a2a45d90'
	// 	const competenceDeleteRes = await requestWithSupertest.delete(
	// 		`/api/competence/${competenceDelete}`
	// 	)
	//
	// 	expect(competenceDeleteRes.statusCode).toEqual(200)
	// })

	/* 
      -------------------------------------------------------------------------------
                            IHEB SLIMEN TESTS
      ------------------------------------------------------------------------------------ */

	///// signup tests

	//// assign challenge to players

	///// crud place test

	/////
	/// view player test

	//// challenge list & done test
})
